package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"strings"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

// S3Wrapper is a struct representing the AWS S3 wrapper
type S3Wrapper struct {
	client *s3.Client
}

// NewS3Wrapper initializes a new instance of the S3Wrapper
func NewS3Wrapper(region string) (*S3Wrapper, error) {
	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithRegion(region))
	if err != nil {
		return nil, err
	}

	client := s3.NewFromConfig(cfg)
	return &S3Wrapper{client: client}, nil
}

// UploadObject uploads a file to the specified S3 bucket and key
func (s *S3Wrapper) UploadObject(bucket, key string, file io.Reader) error {
	_, err := s.client.PutObject(context.TODO(), &s3.PutObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		Body:   file,
	})
	return err
}

// DownloadObject downloads an object from the specified S3 bucket and key
func (s *S3Wrapper) DownloadObject(bucket, key string, dest io.Writer) error {
	resp, err := s.client.GetObject(context.TODO(), &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	_, err = io.Copy(dest, resp.Body)
	return err
}

// DeleteObject deletes an object from the specified S3 bucket and key
func (s *S3Wrapper) DeleteObject(bucket, key string) error {
	_, err := s.client.DeleteObject(context.TODO(), &s3.DeleteObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	return err
}

func main() {
	// Create an instance of the S3Wrapper
	region := "us-west-1" // Specify your desired AWS region
	wrapper, err := NewS3Wrapper(region)
	if err != nil {
		fmt.Println("Error creating S3Wrapper:", err)
		return
	}

	// Example usage: Upload an object
	bucket := "awsgopractice"   // Specify your bucket name
	key := "example-object.txt" // Specify the key for the object
	fileContent := "Hello, S3!"
	err = wrapper.UploadObject(bucket, key, strings.NewReader(fileContent))
	if err != nil {
		fmt.Println("Error uploading object:", err)
		return
	}

	// Example usage: Download an object
	dest := new(bytes.Buffer)
	err = wrapper.DownloadObject(bucket, key, dest)
	if err != nil {
		fmt.Println("Error downloading object:", err)
		return
	}

	fmt.Println("Downloaded content:", dest.String())

	// Example usage: Delete an object
	err = wrapper.DeleteObject(bucket, key)
	if err != nil {
		fmt.Println("Error deleting object:", err)
		return
	}

	fmt.Println("Object deleted successfully!")
}
