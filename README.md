# AWS S3 Wrapper in Go

This repository contains a simple AWS S3 wrapper implemented in Go. It provides a convenient interface to upload, download, and delete objects from an S3 bucket using the official AWS SDK for Go.

## Features

- Upload objects to an S3 bucket.
- Download objects from an S3 bucket.
- Delete objects from an S3 bucket.

## Prerequisites

Before using this wrapper, make sure you have the following prerequisites:

- Go programming language installed (version 1.16+).
- AWS account with S3 access and proper IAM permissions.
- AWS CLI installed and configured with your AWS credentials.